import QtQuick 2.12
import loginui1 1.0
import QtQuick.Controls 2.3
import QtQuick.Timeline 1.0

Rectangle {
    id: root
    state: "loginState"
    width: Constants.width
    height: Constants.height

    Rectangle {
        id: loginPage
        color: "#ffffff"
        anchors.fill: parent

        Text {
            id: pageTitle
            x: 261
            text: qsTr("Qt Account")
            anchors.top: parent.top
            anchors.topMargin: 70
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 24
            font.family: Constants.font.family
        }

        Image {
            id: logo
            width: 100
            height: 100
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            source: "../../../Pictures/qt_logo_green_64x64px.png"
            fillMode: Image.PreserveAspectFit
        }

        Column {
            id: buttonColumn
            x: 260
            y: 329
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 50
            spacing: 5

            PushButton {
                id: loginButton
                width: 120
                text: qsTr("Log In")
                opacity: 0
            }

            PushButton {
                id: registerButton
                width: 120
                text: qsTr("Create Account")
            }
        }

        PushButton {
            id: backButton
            x: 524
            width: 40
            text: "<"
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 20
            font.pixelSize: 24
        }
    }

    TextField {
        id: verifyPasswordField
        x: 170
        width: 300
        text: qsTr("")
        opacity: 1
        anchors.horizontalCenter: passwordField.horizontalCenter
        anchors.top: passwordField.bottom
        anchors.topMargin: 5
        placeholderText: "Verify password"
    }

    TextField {
        id: passwordField
        x: 170
        width: 300
        text: qsTr("")
        anchors.horizontalCenter: usernameField.horizontalCenter
        anchors.top: usernameField.bottom
        anchors.topMargin: 5
        placeholderText: "Password"
    }

    TextField {
        id: usernameField
        x: 170
        width: 300
        text: qsTr("")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 200
        placeholderText: qsTr("Username")
    }

    Connections {
        target: registerButton
        onClicked: {
            root.state = "registerState"
        }
    }

    Connections {
        target: backButton
        onClicked: {
            root.state = "loginState"
        }
    }

    Timeline {
        id: timeline
        animations: [
            TimelineAnimation {
                id: toLoginState
                loops: 1
                duration: 1000
                running: false
                to: 1000
                from: 0
            },
            TimelineAnimation {
                id: toRegisterState
                loops: 1
                duration: 1000
                running: false
                to: 0
                from: 1000
            }
        ]
        startFrame: 0
        endFrame: 1000
        enabled: true

        KeyframeGroup {
            target: backButton
            property: "opacity"
            Keyframe {
                value: 0
                frame: 1000
            }
        }

        KeyframeGroup {
            target: verifyPasswordField
            property: "opacity"
            Keyframe {
                value: 0
                frame: 1000
            }

            Keyframe {
                value: 1
                frame: 0
            }
        }

        KeyframeGroup {
            target: loginButton
            property: "opacity"
            Keyframe {
                value: 1
                frame: 1000
            }

            Keyframe {
                value: 0
                frame: 0
            }
        }

        KeyframeGroup {
            target: verifyPasswordField
            property: "anchors.topMargin"
            Keyframe {
                easing.bezierCurve: [0.39, 0.575, 0.565, 1, 1, 1]
                value: -40
                frame: 1000
            }
        }
    }
    states: [
        State {
            name: "loginState"

            PropertyChanges {
                target: timeline
                enabled: true
            }

            PropertyChanges {
                target: toLoginState
                running: true
            }

            PropertyChanges {
                target: toRegisterState
            }
        },
        State {
            name: "registerState"

            PropertyChanges {
                target: timeline
                enabled: true
            }

            PropertyChanges {
                target: toRegisterState
                running: true
            }

            PropertyChanges {
                target: toLoginState
            }
        }
    ]
}

/*##^##
Designer {
    D{i:2;anchors_y:226}D{i:3;anchors_x:23;anchors_y:21}D{i:5;timeline_expanded:true}
D{i:7;anchors_y:20;timeline_expanded:true}D{i:1;anchors_height:200;anchors_width:200;anchors_x:230;anchors_y:30}
D{i:8;anchors_y:290;timeline_expanded:true}D{i:9;anchors_y:245}D{i:10;anchors_y:200}
D{i:13}
}
##^##*/

